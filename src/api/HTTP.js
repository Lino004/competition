import axios from 'axios';
import config from './config';
// import store from '../store';

const HTTP = axios.create();

HTTP.interceptors.request.use((request) => {
  // eslint-disable-next-line no-param-reassign
  request.headers = {
    'Content-Type': 'application/json',
    // Authorization: `Bearer ${store.getters['user/token']}`,
  };
  if (request.url.indexOf('http') === -1) {
    // eslint-disable-next-line no-param-reassign
    request.url = config.BASE_URL + request.url;
  }
  return request;
}, (error) => Promise.reject(error));

export default HTTP;
