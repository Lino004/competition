import HTTP from './HTTP';

const URL_BASE = 'competition';

export async function GetProfile(id) {
  const response = await HTTP.get(`${URL_BASE}/${id}`);
  return response;
}

export async function GetListCompetition(id, page) {
  const response = await HTTP.get(`${URL_BASE}/${id}/videos/open`, {
    params: {
      page,
    },
  });
  return response;
}

export const NOTHING = '';
