const configProd = {
  BASE_URL: 'https://api.staging.onesongtwoshows.com/v2/',
};
const configDev = {
  BASE_URL: 'https://api.staging.onesongtwoshows.com/v2/',
};

// eslint-disable-next-line import/no-mutable-exports
let config = configDev;

if (process.env.NODE_ENV === 'production') {
  config = configProd;
}

export default config;
