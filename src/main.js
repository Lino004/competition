import Vue from 'vue';
import Buefy from 'buefy';
import device from 'vue-device-detector';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import './assets/scss/app.scss';

Vue.use(Buefy);
Vue.use(device);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
